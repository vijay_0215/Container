<?php
namespace PHPSTORM_META {
    use Psr\Container\ContainerInterface;
    use DreamCat\Container\DcContainerInterface;

    override(
        ContainerInterface::get(0),
        type(0)
    );

    override(
        DcContainerInterface::make(0),
        type(0)
    );
}
