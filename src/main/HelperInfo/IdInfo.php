<?php

namespace DreamCat\Container\HelperInfo;

use DreamCat\Container\EntryFactory;

/**
 * id信息
 * @author vijay
 */
class IdInfo
{
    /** @var string 完整的原始id */
    private $fullId;
    /** @var string|EntryFactory 构建用的类名、工厂名或工厂对象 */
    private $builder;
    /** @var string 附加参数 */
    private $arg;
    /** @var bool 实例是否保留在容器中 */
    private $save;
    /** @var bool 是否重新构建 */
    private $rebuild;
    /** @var bool 构建模式是工厂模式 */
    private $isFactory;
    /** @var string[] 构建后的赋值属性列表 */
    private $props;

    /**
     * IdInfo constructor.
     * @param string $id 原始id
     */
    public function __construct(string $id)
    {
        $tmp = explode("|", $id, 2);
        if (count($tmp) == 2) {
            $prefix = $tmp[0];
            $tmp = $tmp[1];
        } else {
            $prefix = "";
            $tmp = $tmp[0];
        }
        $this->fullId = $tmp;
        $tmp = explode("::", $tmp);
        $this->builder = $tmp[0];
        $this->arg = $tmp[1] ?? "";
        if ($prefix) {
            $this->parsePrefix($prefix);
        }
    }

    /**
     * 解析前缀
     * @param string $prefix 前缀
     * @return void
     */
    protected function parsePrefix(string $prefix)
    {
        if (strpos($prefix, "c") !== false) {
            $this->save = false;
        }
        if (strpos($prefix, "r") !== false) {
            $this->rebuild = true;
        }
    }

    /**
     * 完整的原始id
     * @return string 完整的原始id
     */
    public function getFullId(): string
    {
        return $this->fullId;
    }

    /**
     * 实例是否保留在容器中
     * @return bool 实例是否保留在容器中
     */
    public function isSave(): bool
    {
        return $this->save === null ? true : $this->save;
    }

    /**
     * 实例是否保留在容器中
     * @param bool $save 实例是否保留在容器中
     * @return static 对象本身
     */
    public function setSave(bool $save): IdInfo
    {
        if ($this->save === null) {
            $this->save = $save;
        }
        return $this;
    }

    /**
     * 是否重新构建
     * @return bool 是否重新构建
     */
    public function isRebuild(): bool
    {
        return $this->rebuild === null ? false : $this->rebuild;
    }

    /**
     * 构建模式是工厂模式
     * @return bool 构建模式是工厂模式
     */
    public function isFactory(): bool
    {
        return $this->isFactory === null ? false : $this->isFactory;
    }

    /**
     * 设置构建模式是工厂模式
     * @param bool $isFactory 构建模式是工厂模式
     * @return static 对象本身
     */
    public function setIsFactory(bool $isFactory): IdInfo
    {
        $this->isFactory = $isFactory;
        return $this;
    }

    /**
     * 通过调用类获取实例key
     * @return string 实例key
     */
    public function getInstanceKey(): string
    {
        $builder = is_object($this->getBuilder()) ? get_class($this->getBuilder()) : $this->getBuilder();
        $suff = $this->getArgSuffix();
        if (!strlen($suff)) {
            $suff = "::";
        }
        return $builder . $suff;
    }

    /**
     * 构建用的类名、工厂名或工厂对象
     * @return string|EntryFactory 构建用的类名、工厂名或工厂对象
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * 设置构建用的类名、工厂名或工厂对象
     * @param EntryFactory|string $builder 构建用的类名、工厂名或工厂对象
     * @return static 对象本身
     */
    public function setBuilder($builder): IdInfo
    {
        $this->builder = $builder;
        return $this;
    }

    /**
     * 获取附加参数后缀写法
     * @return string 附加参数后缀写法
     */
    public function getArgSuffix(): string
    {
        return strlen($this->getArg()) ? "::{$this->getArg()}" : "";
    }

    /**
     * 附加参数
     * @return string 附加参数
     */
    public function getArg(): string
    {
        return $this->arg;
    }

    /**
     * 构建后的赋值属性列表
     * @return RegeditProp[] 构建后的赋值属性列表
     */
    public function getProps(): array
    {
        return $this->props === null ? [] : $this->props;
    }

    /**
     * 设置构建后的赋值属性列表
     * @param RegeditProp[] $props 构建后的赋值属性列表
     * @return static 对象本身
     */
    public function setProps(array $props): IdInfo
    {
        $this->props = $props;
        return $this;
    }
}

# end of file
