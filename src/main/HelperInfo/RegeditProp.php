<?php

namespace DreamCat\Container\HelperInfo;

/**
 * 注册时的属性信息
 * @author vijay
 */
class RegeditProp
{
    /** @var bool 是否值表示法 */
    private $isVal;
    /** @var string 值或id */
    private $val;

    /**
     * RegeditProp constructor.
     * @param bool $isVal 是否值表示法
     * @param string $val 值或id
     */
    public function __construct(bool $isVal, string $val)
    {
        $this->isVal = $isVal;
        $this->val = $val;
    }

    /**
     * 通过数组构建属性列表
     * @param array $props 数组表示的属性列表
     * @return RegeditProp[] 属性列表
     */
    public static function builderProps(array $props): array
    {
        $arr = [];
        foreach ($props as $name => $prop) {
            if (is_array($prop)) {
                if (isset($prop["val"])) {
                    $arr[$name] = new RegeditProp($prop["isVal"] ?? true, $prop["val"]);
                }
            } else {
                $arr[$name] = new RegeditProp(true, $prop);
            }
        }
        return $arr;
    }

    /**
     * 是否值表示法
     * @return bool 是否值表示法
     */
    public function isVal(): bool
    {
        return $this->isVal;
    }

    /**
     * 值或id
     * @return string 值或id
     */
    public function getVal()
    {
        return $this->val;
    }
}

# end of file
