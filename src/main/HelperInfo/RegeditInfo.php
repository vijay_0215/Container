<?php

namespace DreamCat\Container\HelperInfo;

use DreamCat\Container\EntryFactory;

/**
 * 容器注册信息
 * @author vijay
 */
class RegeditInfo
{
    /** @var bool 构建模式是工厂模式 */
    private $isFactory;
    /** @var string|EntryFactory 构建用的类名、工厂名或工厂对象 */
    private $builder;
    /** @var bool 实例是否保留在容器中 */
    private $save;
    /** @var string[] 构建后的赋值属性列表 */
    private $props;

    /**
     * RegeditInfo constructor.
     * @param bool $isFactory 构建模式是工厂模式
     * @param string|EntryFactory $builder 构建用的类名、工厂名或工厂对象
     * @param RegeditProp[] $props 构建后的赋值属性列表
     * @param bool $save 实例是否保留在容器中
     */
    public function __construct(bool $isFactory, $builder, array $props, bool $save)
    {
        $this->isFactory = $isFactory;
        if ($isFactory && !is_subclass_of($builder, EntryFactory::class)) {
            throw new \RuntimeException("Bean工厂类 {$builder} 必须实现" . EntryFactory::class);
        }
        $this->builder = $builder;
        $this->props = $props;
        $this->save = $save;
    }

    /**
     * 构建模式是工厂模式
     * @return bool 构建模式是工厂模式
     */
    public function isFactory(): bool
    {
        return $this->isFactory;
    }

    /**
     * 构建用的类名、工厂名或工厂对象
     * @return string|EntryFactory 构建用的类名、工厂名或工厂对象
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * 实例是否保留在容器中
     * @return bool 实例是否保留在容器中
     */
    public function isSave(): bool
    {
        return $this->save;
    }

    /**
     * 构建后的赋值属性列表
     * @return RegeditProp[] 构建后的赋值属性列表
     */
    public function getProps(): array
    {
        return $this->props;
    }
}

# end of file
