<?php

namespace DreamCat\Container\HelperInfo;

use DreamCat\Container\EntryFactory;

/**
 * 注册信息工厂
 * @author vijay
 */
class RegeditFactory
{
    /**
     * 生成注册实体类名的注册信息
     * @param string $className 实体类名
     * @param array $props 构建后的赋值属性列表，结构参考regeditFactory
     * @param bool $save 实例是否保留在容器中
     * @return RegeditInfo 注册信息
     * @see regeditFactory
     */
    public static function regeditClass(
        string $className,
        array $props = [],
        bool $save = true
    ): RegeditInfo {
        return new RegeditInfo(
            false,
            $className,
            RegeditProp::builderProps($props),
            $save
        );
    }

    /**
     * 生成注册实体工厂方法的注册信息
     * @param EntryFactory $entryFactory 工厂方法
     * @param array $props 构建后的赋值属性列表，键是属性名，
     *  元素$val如果非数组则相当于{isVal:true,val:$val}，如果是数组{
     *      isVal : bool 选填，默认为true 表示是否直接将值给构建后对象的属性，否则从容器中get相应元素
     *      val : 值或实例id
     * }
     * @param bool $save 实例是否保留在容器中
     * @return RegeditInfo 注册信息
     */
    public static function regeditFactory(
        EntryFactory $entryFactory,
        array $props = [],
        bool $save = true
    ): RegeditInfo {
        return new RegeditInfo(
            true,
            $entryFactory,
            RegeditProp::builderProps($props),
            $save
        );
    }

    /**
     * 生成注册实体工厂类名的注册信息
     * @param string $entryFactoryClass 工厂类
     * @param array $props 构建后的赋值属性列表，结构参考regeditFactory
     * @param bool $save 实例是否保留在容器中
     * @return RegeditInfo 注册信息
     * @see regeditFactory
     */
    public static function regeditFactoryClass(
        string $entryFactoryClass,
        array $props = [],
        bool $save = true
    ): RegeditInfo {
        return new RegeditInfo(
            true,
            $entryFactoryClass,
            RegeditProp::builderProps($props),
            $save
        );
    }
}

# end of file
