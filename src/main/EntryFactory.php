<?php

namespace DreamCat\Container;

/**
 * 实体创建工厂接口
 * @author vijay
 */
interface EntryFactory
{
    /**
     * 创建实体
     * @param string $id 实体标识
     * @param string $arg 额外参数
     * @return mixed 实体
     */
    public function create(string $id, string $arg);
}

# end of file
