<?php

namespace DreamCat\Container;

use DreamCat\Container\HelperInfo\RegeditInfo;

/**
 * 可注册内容的容器接口
 * @author vijay
 */
interface RegeditContainer extends DcContainerInterface
{
    /**
     * 注册创建模式
     * @param string $id 实体标识
     * @param RegeditInfo $info 注册信息
     * @param bool $overwrite 是否覆盖原有设定
     * @return static 容器本身
     */
    public function regedit(string $id, RegeditInfo $info, bool $overwrite = true): RegeditContainer;

    /**
     * 直接注册一个实体对象
     * @param string $id 实体标识
     * @param mixed $object 实体对象
     * @param bool $overwrite 是否覆盖原有设定
     * @return static 容器本身
     */
    public function regeditInstance(string $id, $object, bool $overwrite = true): RegeditContainer;

    /**
     * 注册自定义注解处理器
     * @param string $annotation 注解名称
     * @param AnnotationDealer $annotationDealer 处理器
     * @return static 容器本身
     */
    public function regeditAnnotation(string $annotation, AnnotationDealer $annotationDealer): RegeditContainer;

    /**
     * 关闭容器，销毁时调用，清理资源，释放句柄
     * @return void
     */
    public function close(): void;

    /**
     * 容器是否关闭，关闭情况下无法执行get操作
     * @return bool
     */
    public function isClosed(): bool;
}

# end of file
