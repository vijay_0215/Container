<?php

namespace DreamCat\Container;

/**
 * 自定义注解处理器
 * @author vijay
 */
interface AnnotationDealer
{
    /**
     * get
     * 获取注解生成的值
     * @param Container $container 容器
     * @param array $args 注解参数
     * @return mixed 生成的值
     */
    public function get(Container $container, array $args);
}

# end of file
