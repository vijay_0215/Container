<?php
declare(strict_types=1);

namespace DreamCat\Container;

use Psr\Container\ContainerInterface;

/**
 * 容器扩展接口
 * @author vijay
 */
interface DcContainerInterface extends ContainerInterface
{
    /**
     * 利用容器构建对象，但是不做保存也不尝试取已构建的实例
     * @param string $className 类名
     * @param array|null $args 构造函数的参数列表，如果传null则使用自动判断
     * @return mixed 实例
     */
    public function make(string $className, array $args = null);
}

# end of file
