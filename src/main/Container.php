<?php

namespace DreamCat\Container;

use DreamCat\Container\EntryLife\EntryOnClose;
use DreamCat\Container\EntryLife\EntryOnInit;
use DreamCat\Container\Exception\ContainerClosed;
use DreamCat\Container\Exception\NotFoundClass;
use DreamCat\Container\Exception\RegeditError;
use DreamCat\Container\HelperInfo\IdInfo;
use DreamCat\Container\HelperInfo\RegeditInfo;
use DreamCat\Container\HelperInfo\RegeditProp;
use DreamCat\Container\Utils\KeyAnnotation;
use DreamCat\OptionStruct\Option;
use Dreamcat\PropertyAnalysis\Pojo\TypeDescription;
use Dreamcat\PropertyAnalysis\PropertyAnalysis;
use Dreamcat\PropertyAnalysis\PropertyAnalysisInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionClass;
use ReflectionProperty;

/**
 * 容器类
 * @author vijay
 */
class Container implements RegeditContainer
{
    /** @var array 已创建的实例 */
    private $instanceList = [];

    /** @var RegeditInfo[] 标识定义 */
    private $definitionList = [];

    /** @var AnnotationDealer[] 自定义注解处理器 */
    private $dealers = [];

    /** @var bool 容器是否被关闭 */
    private $isClosed = false;
    /** @var PropertyAnalysisInterface 类的属性分析器 */
    private $propertyAnalysis;

    /**
     * 注册创建模式
     * @param string $id 实体标识
     * @param RegeditInfo $info 注册信息
     * @param bool $overwrite 是否覆盖原有设定
     * @return static 容器本身
     */
    public function regedit(string $id, RegeditInfo $info, bool $overwrite = true): RegeditContainer
    {
        if ($overwrite || !isset($this->definitionList[$id])) {
            $this->definitionList[$id] = $info;
        }
        return $this;
    }

    /**
     * 直接注册一个实体对象
     * @param string $id 实体标识
     * @param mixed $object 实体对象
     * @param bool $overwrite 是否覆盖原有设定
     * @return static 容器本身
     */
    public function regeditInstance(string $id, $object, bool $overwrite = true): RegeditContainer
    {
        if ($overwrite || !isset($this->instanceList[$id])) {
            $this->instanceList[$id] = $object;
        }
        return $this;
    }

    /**
     * 注册自定义注解处理器
     * @param string $annotation 注解名称
     * @param AnnotationDealer $annotationDealer 处理器
     * @return static 容器本身
     */
    public function regeditAnnotation(string $annotation, AnnotationDealer $annotationDealer): RegeditContainer
    {
        $this->dealers[$annotation] = $annotationDealer;
        return $this;
    }

    /**
     * 如果容器内有标识符对应的内容时，返回 true 。
     * 否则，返回 false。
     *
     * 调用 `has($id)` 方法返回 true，并不意味调用  `get($id)` 不会抛出异常。
     * 而只意味着 `get($id)` 方法不会抛出 `NotFoundExceptionInterface` 实现类的异常。
     *
     * @param string $id 查找的实体标识符字符串。
     *
     * @return bool
     */
    public function has($id): bool
    {
        # todo 优化使之符合规定
        if ($this->isClosed()) {
            return false;
        }
        if (isset($this->instanceList[$id])) {
            return true;
        }
        if (isset($this->definitionList[$id])) {
            return true;
        }
        return false;
    }

    /**
     * 容器是否关闭，关闭情况下无法执行get操作
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->isClosed;
    }

    /**
     * 析构函数
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * 关闭容器，销毁时调用，清理资源，释放句柄
     * @return void
     */
    public function close(): void
    {
        if ($this->isClosed) {
            return;
        }
        $this->isClosed = true;
        $closed = [];
        foreach ($this->instanceList as $key => $item) {
            if (!($item instanceof EntryOnClose)) {
                continue;
            }
            $id = spl_object_id($item);
            if (!isset($closed[$id])) {
                $closed[$id] = $item;
                $item->entryOnClose();
            }
        }
    }

    /**
     * 根据注解生成属性的值
     * @param array $keyWords 属性注解上的关键字
     * @param TypeDescription|null $typeDescription 类型描述
     * @return Option 值
     */
    protected function getPropertyValue(array $keyWords, ?TypeDescription $typeDescription): Option
    {
        # 判断注解
        if (isset($keyWords[KeyAnnotation::AUTOWIRE])) {
            $autowireKey = implode(" ", $keyWords[KeyAnnotation::AUTOWIRE]);
            if (strlen($autowireKey)) {
                return Option::some($this->get($autowireKey));
            }
            if (!$typeDescription) {
                return Option::none();
            }
            $type = $typeDescription->getPropertyType();
            if ($typeDescription->isClass() || $typeDescription->isInterface()) {
                return Option::some($this->get($type->name));
            }
        } elseif ($keyWords) {
            $key = array_keys($keyWords)[0];
            $value = $this->getDealers()[$key]->get($this, $keyWords[$key]);
            if ($value !== null) {
                return Option::some($value);
            }
        }
        return Option::none();
    }

    /**
     * 在容器中查找并返回实体标识符对应的对象。
     * @param string $id 查找的实体标识符字符串。
     * @return mixed 查找到的对象。
     * @throws ContainerExceptionInterface 查找对象过程中发生了其他错误时抛出的异常。
     * @throws NotFoundExceptionInterface  容器中没有实体标识符对应对象时抛出的异常。
     */
    public function get($id)
    {
        if ($this->isClosed()) {
            throw new ContainerClosed();
        }
        return $this->getByIdInfo($this->getIdInfo($id));
    }

    /**
     * 在容器中查找并返回实体标识符对应的对象
     * @param IdInfo $idInfo id信息
     * @return mixed 查找到的对象。
     * @throws ContainerExceptionInterface 查找对象过程中发生了其他错误时抛出的异常。
     * @throws NotFoundExceptionInterface  容器中没有实体标识符对应对象时抛出的异常。
     */
    protected function getByIdInfo(IdInfo $idInfo)
    {
        $instance = $this->returnInstance($idInfo);
        if ($instance->isDefined()) {
            return $instance->get();
        }

        $found = [];
        if (!$idInfo->isFactory()) {
            do {
                $lastBuilder = $idInfo->getBuilder();
                if (isset($found[$lastBuilder])) {
                    throw new RegeditError("添加的注册产生循环");
                }
                $found[$lastBuilder] = 1;
                if (!$idInfo->isRebuild() && isset($this->instanceList[$idInfo->getInstanceKey()])) {
                    return $this->instanceList[$idInfo->getInstanceKey()];
                }
                if (isset($this->definitionList[$lastBuilder])) {
                    $idInfo = $this->getBuilderInfo($idInfo, $this->definitionList[$lastBuilder]);
                }
            } while ($this->goSearch($idInfo, $lastBuilder));
        }
        return $this->createEntry($idInfo);
    }

    /**
     * 判断是否需要直接返回实例
     * @param IdInfo $idInfo id信息
     * @return Option
     */
    protected function returnInstance(IdInfo $idInfo): Option
    {
        # 如果存在已使用过的完整实例则返回
        if (!$idInfo->isRebuild() && isset($this->instanceList[$idInfo->getFullId()])) {
            return Option::some($this->instanceList[$idInfo->getFullId()]);
        }
        return Option::none();
    }

    /**
     * 获取注册信息中的内容
     * @param IdInfo $idInfo id信息
     * @param RegeditInfo $builder 注册信息
     * @return IdInfo 更新后的id信息
     */
    protected function getBuilderInfo(IdInfo $idInfo, RegeditInfo $builder)
    {
        return $idInfo->setBuilder($builder->getBuilder())
            ->setIsFactory($builder->isFactory())
            ->setSave($builder->isSave())
            ->setProps($builder->getProps());
    }

    /**
     * 是否继续搜索
     * @param IdInfo $idInfo id信息
     * @param string $lastBuilder 上次使用的builder
     * @return bool 继续搜索
     */
    protected function goSearch($idInfo, $lastBuilder): bool
    {
        if ($idInfo->isFactory()) {
            # 是工厂模型，可以开始创建对象了
            return false;
        }
        if ($lastBuilder != $idInfo->getBuilder() && isset($this->definitionList[$idInfo->getBuilder()])) {
            # 依然存在别名，继续换别名且不一样
            return true;
        }
        return !$idInfo->isRebuild() && isset($this->instanceList[$idInfo->getInstanceKey()]);
    }

    /**
     * 创建实体
     * @param IdInfo $idInfo 实体标识符
     * @return mixed 实体
     * @throws ContainerExceptionInterface 查找对象过程中发生了其他错误时抛出的异常。
     * @throws NotFoundExceptionInterface  容器中没有实体标识符对应对象时抛出的异常。
     */
    protected function createEntry($idInfo)
    {
        if ($idInfo->isFactory()) {
            $factory = $idInfo->getBuilder();
            if (!$factory instanceof EntryFactory) {
                $factory = $this->createByClassName($factory, $idInfo->getProps());
            }
            $obj = $factory->create($idInfo->getFullId(), $idInfo->getArg());
        } else {
            $obj = $this->createByClassName($idInfo->getBuilder(), $idInfo->getProps(), $idInfo->isRebuild());
        }
        if ($idInfo->isSave()) {
            # 保留创建的实例
            $this->instanceList[$idInfo->getFullId()] = $obj;
            if (is_object($obj)) {
                # 确保类名本体留下记录
                $insKey = get_class($obj) . $idInfo->getArgSuffix();
                if (!isset($this->instanceList[$insKey])) {
                    $this->instanceList[$insKey] = $obj;
                }
            }
            if ($idInfo->isFactory()) {
                $insKey = $idInfo->getInstanceKey();
                if (!isset($this->instanceList[$insKey])) {
                    $this->instanceList[$insKey] = $obj;
                }
            }
        }
        return $obj;
    }

    /**
     * 根据类名创建实体
     * @param string $className 类名
     * @param RegeditProp[] $props 构建后的属性列表
     * @param bool $rebuild 是否强制重建
     * @return mixed 实体
     * @throws ContainerExceptionInterface 查找对象过程中发生了其他错误时抛出的异常。
     * @throws NotFoundExceptionInterface  容器中没有实体标识符对应对象时抛出的异常。
     */
    protected function createByClassName(string $className, array $props, bool $rebuild = false)
    {
        if (!class_exists($className)) {
            throw new NotFoundClass($className);
        }
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionClass = new ReflectionClass($className);
        if (!$rebuild) {
            $fullName = $reflectionClass->getName();
            if (isset($this->instanceList[$fullName])) {
                return $this->instanceList[$fullName];
            }
        }
        # 分析构造函数，将构造函数需要的参数做创建，并调用构造函数，生成实例
        $obj = $this->createByConstruct($reflectionClass);
        # 遍历属性，依据注解，赋值
        $obj = $this->injectionProperty($reflectionClass, $obj);
        # 将属性赋值
        $obj = $this->assignmentProperty($obj, $props);
        if ($obj instanceof EntryOnInit) {
            $obj->entryOnInit();
        }
        return $obj;
    }

    /**
     * 分析构造函数，将构造函数需要的参数做创建，并调用构造函数，生成实例
     * @param ReflectionClass $reflectionClass 反射类
     * @return mixed 生成后的实例
     */
    protected function createByConstruct(ReflectionClass $reflectionClass)
    {
        $construct = $reflectionClass->getConstructor();
        $args = [];
        if ($construct) {
            $builtinMap = [
                "int" => 0,
                "float" => 0,
                "string" => "",
                "bool" => false,
                "array" => [],
                "callable" => function () {
                },
            ];
            foreach ($construct->getParameters() as $parameter) {
                if ($parameter->isDefaultValueAvailable()) {
                    /** @noinspection PhpUnhandledExceptionInspection */
                    $args[] = $parameter->getDefaultValue();
                } else {
                    $typeInfo = $parameter->getType();
                    if ($typeInfo) {
                        if ($typeInfo->isBuiltin()) {
                            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
                            $args[] = $builtinMap[$typeInfo->getName()] ?? null;
                        } else {
                            /** @noinspection PhpPossiblePolymorphicInvocationInspection */
                            $args[] = $this->get($typeInfo->getName());
                        }
                    } else {
                        $args[] = null;
                    }
                }
            }
        }
        # 调用构造函数，生成实例
        return $reflectionClass->newInstanceArgs($args);
    }

    /**
     * 注入属性
     * @param ReflectionClass $class 类的反射
     * @param mixed $obj 实体对象
     * @return mixed 注入后的实体对象
     */
    protected function injectionProperty(ReflectionClass $class, $obj)
    {
        foreach ($this->getPropertyAnalysis()->analysis($class)->getPropertyList() as $propertyResult) {
            $typeDescription = null;
            if ($propertyResult->isHasSetter()) {
                $typeDescription = $propertyResult->getSetterTypeDescription();
            }
            if (!$typeDescription) {
                $typeDescription = $propertyResult->getTypeDescription();
            }
            $keyWords = $propertyResult->getOtherKeywordResult();
            $value = $this->getPropertyValue($keyWords, $typeDescription);
            if ($value->isEmpty()) {
                continue;
            }
            $value = $value->get();
            if ($propertyResult->isHasSetter()) {
                $propertyResult->getSetter()->invoke($obj, $value);
            } else {
                $property = $propertyResult->getProperty();
                $allow = $property->isPublic();
                if (!$allow) {
                    $property->setAccessible(true);
                }
                $property->setValue($obj, $value);
                if (!$allow) {
                    $property->setAccessible(false);
                }
            }
        }
        return $obj;
    }

    /**
     * @return PropertyAnalysisInterface 类的属性分析器
     */
    public function getPropertyAnalysis(): PropertyAnalysisInterface
    {
        if (!$this->propertyAnalysis) {
            $this->propertyAnalysis = new PropertyAnalysis();
            $this->propertyAnalysis->addVarKeyWord(KeyAnnotation::AUTOWIRE);
            foreach (array_keys($this->getDealers()) as $key) {
                $this->propertyAnalysis->addVarKeyWord($key);
            }
        }
        return $this->propertyAnalysis;
    }

    /**
     * @param PropertyAnalysisInterface $propertyAnalysis 类的属性分析器
     * @return static 类的属性分析器
     */
    public function setPropertyAnalysis(PropertyAnalysisInterface $propertyAnalysis): Container
    {
        $this->propertyAnalysis = $propertyAnalysis;
        return $this;
    }

    /**
     * @return AnnotationDealer[] 自定义注解处理器
     */
    protected function getDealers(): array
    {
        return $this->dealers;
    }

    /**
     * @param mixed $obj 实体对象
     * @param RegeditProp[] $props 构建后的属性列表
     * @return mixed 实体
     */
    protected function assignmentProperty($obj, array $props)
    {
        foreach ($props as $name => $prop) {
            $val = $prop->isVal() ? $prop->getVal() : $this->get($prop->getVal());
            if (property_exists($obj, $name)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $property = new ReflectionProperty($obj, $name);
                $accessible = $property->isPublic();
                if (!$accessible) {
                    $property->setAccessible(true);
                }
                $property->setValue($obj, $val);
                if (!$accessible) {
                    $property->setAccessible(false);
                }
            } else {
                /** @noinspection PhpVariableVariableInspection */
                $obj->$name = $val;
            }
        }
        return $obj;
    }

    /**
     * 解析ID信息
     * @param string $id 完整id
     * @return IdInfo ID信息
     */
    protected function getIdInfo(string $id)
    {
        return new IdInfo($id);
    }

    /**
     * @inheritDoc
     * @throws NotFoundClass
     */
    public function make(string $className, array $args = null)
    {
        if (!class_exists($className)) {
            throw new NotFoundClass($className);
        }
        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionClass = new ReflectionClass($className);
        if ($args === null) {
            # 分析构造函数，将构造函数需要的参数做创建，并调用构造函数，生成实例
            $obj = $this->createByConstruct($reflectionClass);
        } else {
            $obj = $reflectionClass->newInstanceArgs($args);
        }
        # 遍历属性，依据注解，赋值
        $obj = $this->injectionProperty($reflectionClass, $obj);
        if ($obj instanceof EntryOnInit) {
            $obj->entryOnInit();
        }
        return $obj;
    }
}

# end of file
