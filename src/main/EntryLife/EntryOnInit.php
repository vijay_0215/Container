<?php

namespace DreamCat\Container\EntryLife;

/**
 * 容器实体被创建时调用
 * @author vijay
 */
interface EntryOnInit
{
    /**
     * 容器实体被创建时调用
     * @return void
     */
    public function entryOnInit(): void;
}

# end of file
