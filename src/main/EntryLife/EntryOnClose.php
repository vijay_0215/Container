<?php

namespace DreamCat\Container\EntryLife;

/**
 * 容器实体被销毁时调用
 * @author vijay
 */
interface EntryOnClose
{
    /**
     * 容器实体被销毁时调用
     * @return void
     */
    public function entryOnClose(): void;
}

# end of file
