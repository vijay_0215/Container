<?php

namespace DreamCat\Container\Exception;

use Psr\Container\NotFoundExceptionInterface;

/**
 * 容器中没有实体标识符对应对象时抛出的异常
 * @author vijay
 */
abstract class NotFoundException extends Error implements NotFoundExceptionInterface
{
}

# end of file
