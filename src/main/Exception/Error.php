<?php

namespace DreamCat\Container\Exception;

/**
 * 项目异常基类
 * @author vijay
 */
abstract class Error extends \Exception
{
}

# end of file
