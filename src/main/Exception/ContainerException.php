<?php

namespace DreamCat\Container\Exception;

use Psr\Container\ContainerExceptionInterface;

/**
 * 查找对象过程中发生了其他错误时抛出的异常
 * @author vijay
 */
abstract class ContainerException extends Error implements ContainerExceptionInterface
{
}

# end of file
