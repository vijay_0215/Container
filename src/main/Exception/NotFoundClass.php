<?php

namespace DreamCat\Container\Exception;

use Throwable;

/**
 * 找不到类
 * @author vijay
 */
class NotFoundClass extends NotFoundException
{
    /** @var int 找不到类名 */
    const NOT_FOUND_CLASS = 1001;

    /**
     * NotFoundClass constructor.
     * @param string $className 类名
     * @param Throwable $previous 异常链中的前一个异常
     */
    public function __construct(string $className, Throwable $previous = null)
    {
        parent::__construct("创建实体时类 {$className} 找不到", self::NOT_FOUND_CLASS, $previous);
    }
}

# end of file
