<?php

namespace DreamCat\Container\Exception;

/**
 * 容器关闭情况下抛出的异常
 * @author vijay
 */
class ContainerClosed extends ContainerException
{
}

# end of file
