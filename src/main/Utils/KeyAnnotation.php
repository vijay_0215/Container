<?php

namespace DreamCat\Container\Utils;

/**
 * 关键注解
 * @author vijay
 */
class KeyAnnotation
{
    /** @var string Autowire 注解 */
    const AUTOWIRE = "Autowire";
}

# end of file
