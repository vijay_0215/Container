<?php /** @noinspection PhpUnusedParameterInspection */

namespace DreamCat\Container;

use DreamCat\Container\DemoClass\EntryWithLife;
use DreamCat\Container\DemoClass\MakeClass;
use DreamCat\Container\DemoClass\SubDir\SubClass1;
use DreamCat\Container\DemoClass\SubDir\SubClass2;
use DreamCat\Container\DemoClass\SubTestCase;
use DreamCat\Container\DemoClass\TestCase1;
use DreamCat\Container\DemoClass\TestCase2;
use DreamCat\Container\DemoClass\TestCase3;
use DreamCat\Container\DemoClass\TestCase5;
use DreamCat\Container\DemoClass\TestFactory;
use DreamCat\Container\DemoClass\TestPrefixClass;
use DreamCat\Container\EntryLife\EntryOnClose;
use DreamCat\Container\EntryLife\EntryOnInit;
use DreamCat\Container\Exception\ContainerClosed;
use DreamCat\Container\Exception\NotFoundClass;
use DreamCat\Container\Exception\RegeditError;
use DreamCat\Container\HelperInfo\RegeditFactory;
use DreamCat\Container\HelperInfo\RegeditInfo;
use Dreamcat\PropertyAnalysis\PropertyAnalysisInterface;
use PHPUnit\Framework\TestCase;
use RuntimeException;

/**
 * 容器测试类
 * @author vijay
 */
class ContainerTest extends TestCase
{
    /**
     * 测试注册工厂后的容器
     * @return void
     */
    public function testFactory()
    {
        $container = new Container();
        $id = uniqid("test-");
        $obj = new \stdClass();
        $obj->id = uniqid("result-");
        $mock = $this->getMockForAbstractClass(EntryFactory::class);
        $mock->expects(static::once())
            ->method("create")
            ->with(static::equalTo($id))
            ->willReturn($obj);
        $container->regedit($id, RegeditFactory::regeditFactory($mock));
        $result = $container->get($id);
        static::assertEquals($obj, $result, "工厂方法未生效");
    }

    /**
     * 测试工厂类直接注册
     * @return void
     */
    public function testFactoryClass()
    {
        $id = uniqid("test-");
        $arg = uniqid("arg");
        $factoryId = uniqid("factory-");
        $obj = new \stdClass();
        $obj->fullId = "{$id}::{$arg}";
        $obj->arg = $arg;
        $obj->factoryId = $factoryId;
        $obj->other = uniqid("other-");
        $first = clone $obj;
        $first->fullId = "alias::{$arg}";
        $first->inited = true;

        TestFactory::$count = 0;

        $container = new Container();
        $container->regedit($id, RegeditFactory::regeditClass("alias"));
        $container->regedit(
            "alias",
            RegeditFactory::regeditFactoryClass(
                TestFactory::class,
                [
                    "id" => $factoryId,
                    "other" => ["val" => $obj->other],
                ]
            )
        );

        $result = $container->get($first->fullId);
        static::assertEquals($first, $result, "工厂类注册未生效");
        $result->arg .= "::ext";
        static::assertEquals($result, $container->get($obj->fullId), "未返回之前创建实例");
    }

    /**
     * 测试实例id前缀
     * @return void
     */
    public function testGetPrefix()
    {
        $container = new Container();
        TestPrefixClass::$idx = 0;
        $obj1 = $container->get("c|" . TestPrefixClass::class);
        $obj2 = $container->get(TestPrefixClass::class);
        $obj3 = $container->get(TestPrefixClass::class);
        $obj4 = $container->get("r|" . TestPrefixClass::class);
        static::assertEquals(1, $obj1->id);
        static::assertEquals(2, $obj2->id);
        static::assertEquals(2, $obj3->id);
        static::assertEquals(3, $obj4->id);
    }

    /**
     * 测试注册实例后的容器
     * @return void
     */
    public function testInstance()
    {
        $container = new Container();
        $id = uniqid("test-");
        $obj = uniqid("result-");
        $container->regeditInstance($id, $obj);
        static::assertEquals($obj, $container->get($id), "未返回事先注入的对象");
    }

    /**
     * 测试常规主流程
     * @return void
     */
    public function testNormal()
    {
        $container = new Container();
        $container->regedit("sub", RegeditFactory::regeditClass(SubClass1::class));
        $container->regedit("sub2", RegeditFactory::regeditClass("\\" . SubClass2::class));
        $configMock = $this->getMockForAbstractClass(AnnotationDealer::class);
        $configMock->expects(static::once())
            ->method("get")
            ->with($container, ["db.username"])
            ->willReturn("devuser");

        $container->regeditAnnotation("Config", $configMock);
        /** @var TestCase2 $obj */
        $obj = $container->get(TestCase2::class);

        static::assertEquals(SubClass2::class, get_class($obj->value1()), "TestCase2的value1类型不正确");
        static::assertEquals(SubClass1::class, get_class($obj->value2()), "TestCase2的value2类型不正确");
        static::assertEquals("devuser", $obj->value1()->dbuser, "TestCase2的value1的dbuser值不正确");
        /** @var SubTestCase $objValue3 */
        $objValue3 = $obj->value3();
        static::assertEquals(SubTestCase::class, get_class($objValue3), "TestCase2的value3类型不正确");
        static::assertEquals(SubClass1::class, get_class($objValue3->sub()), "value3下的subValue类型不正确");
        static::assertEquals($objValue3->sub()->id, $obj->value2()->id, "value3下的subValue与之前的SubClass值不同");

        static::assertEquals(SubClass1::class, get_class($objValue3->public), "value3下的public类型不正确");
        static::assertEquals($obj->value2()->id, $objValue3->public->id, "value3下的public与之前的SubClass值不同");

        static::assertEquals(SubClass2::class, get_class($objValue3->value1()), "value3下的protected类型不正确");
        static::assertEquals($obj->value1()->id, $objValue3->value1()->id, "value3下的protected有重复创建");

        static::assertEquals(\Exception::class, get_class($objValue3->value2()), "value3下的private类型不正确");

        static::assertNull($objValue3->noAutowire, "value3下的非注入属性被注入");
        static::assertEquals(100, $objValue3->noAutoWithDefault, "value3下的非注入属性被修改了默认值");

        static::assertEquals(TestCase1::class, get_class($objValue3->par()), "value3下的parent类型不正确");
        static::assertEquals($objValue3->public, $objValue3->par()->public, "parent下的public值不正确");
        static::assertEquals($objValue3->value1(), $objValue3->par()->value1(), "parent下的protected值不正确");
        static::assertEquals($objValue3->value2(), $objValue3->par()->value2(), "parent下的private值不正确");
        static::assertEquals($objValue3->noAutowire, $objValue3->par()->noAutowire, "parent下的noAutowire值不正确");
        static::assertEquals(
            $objValue3->noAutoWithDefault,
            $objValue3->par()->noAutoWithDefault,
            "parent下的noAutoWithDefault值不正确"
        );

        # 检查TestCase3的构建
        static::assertEquals(TestCase3::class, get_class($obj->value4), "value4类型不正确");
        static::assertTrue(0 === $obj->value4->int, "int 构造赋值失败");
        static::assertTrue(0.0 === $obj->value4->float, "float 构造赋值失败");
        static::assertTrue("" === $obj->value4->str, "str 构造赋值失败");
        static::assertTrue(false === $obj->value4->bool, "bool 构造赋值失败");
        static::assertTrue([] === $obj->value4->array, "array 构造赋值失败");
        static::assertEquals($obj->value1(), $obj->value4->subClass2, "subClass2 构造赋值失败");
        static::assertEquals(10, $obj->value4->intDef, "intDef 构造赋值失败");
        static::assertEquals(5.3, $obj->value4->floatDef, "floatDef 构造赋值失败");
        static::assertEquals("abc", $obj->value4->strDef, "strDef 构造赋值失败");
        static::assertTrue($obj->value4->boolDef, "boolDef 构造赋值失败");
        static::assertEquals(["bbc"], $obj->value4->arrayDef, "arrayDef 构造赋值失败");
    }

    /**
     * 测试新方法容器实体生命周期
     * @return Container 容器
     */
    public function testEntryLife()
    {
        $container = new Container();
        $mockInit = $this->getMockForAbstractClass(EntryOnInit::class);
        $mockInit->expects(self::once())
            ->method("entryOnInit");
        $mockClose = $this->getMockForAbstractClass(EntryOnClose::class);
        $mockClose->expects(self::exactly(1))
            ->method("entryOnClose");

        $container->regeditInstance(EntryOnInit::class, $mockInit);
        $container->regeditInstance(EntryOnClose::class, $mockClose);

        $container->get(EntryWithLife::class);
        self::assertTrue($container->has(EntryOnInit::class), "实体应当存在时判定false");
        self::assertFalse($container->isClosed(), "容器异常关闭");
        $container->close();
        self::assertTrue($container->isClosed(), "容器正确关闭");
        self::assertFalse($container->has(EntryOnInit::class), "容器关闭后依然可以判定实体存在");
        $container->close();
        return $container;
    }

    /**
     * 测试容器被关闭后调用get
     * @param Container $container 被关闭的容器
     * @return void
     * @depends testEntryLife
     */
    public function testCloseGet(Container $container)
    {
        $this->expectException(ContainerClosed::class);
        $container->get("");
    }

    /**
     * 测试带子参数的情况
     * @return void
     */
    public function testChildArg()
    {
        $container = new Container();
        $factory = $this->getMockForAbstractClass(EntryFactory::class);
        $fn = function ($id, $arg) {
            $o = new \stdClass();
            $o->id = $arg;
            return $o;
        };
        $factory->expects(static::exactly(2))
            ->method("create")
            ->withConsecutive(
                [
                    static::equalTo("beanId::1"),
                    static::equalTo("1"),
                ],
                [
                    static::equalTo("beanId::2"),
                    static::equalTo("2"),
                ]
            )
            ->willReturnCallback($fn);
        $container->regedit("beanId", RegeditFactory::regeditFactory($factory));
        self::assertEquals($fn("", "1"), $container->get("beanId::1"));
        self::assertEquals($fn("", "2"), $container->get("beanId::2"));
    }

    /**
     * 测试RegeditError异常
     * @return void
     */
    public function testRegeditError()
    {
        $this->expectException(RegeditError::class);
        $this->expectExceptionMessage("添加的注册产生循环");
        $container = new Container();
        $container->regedit("c1", RegeditFactory::regeditClass("c2"));
        $container->regedit("c2", RegeditFactory::regeditClass("c1"));
        $container->get("c1");
    }

    /**
     * 测试NotFoundClass异常
     * @return void
     */
    public function testNotFoundClass()
    {
        $this->expectException(NotFoundClass::class);
        $container = new Container();
        $container->regedit("c1", RegeditFactory::regeditClass("c2"));
        $container->get("c1");
    }

    /**
     * 测试父子有同名属性的情况
     * @return void
     */
    public function testSameProperty()
    {
        $container = new Container();
        $configMock = $this->getMockForAbstractClass(AnnotationDealer::class);
        $configMock->expects(static::exactly(3))
            ->method("get")
            ->willReturnCallback(
                function ($p1, $p2) {
                    return $p2[0];
                }
            );
        $container->regeditAnnotation("Config", $configMock);
        $str = uniqid("demo-");
        $container->regeditInstance("demo", $str);

        /** @var TestCase5 $obj */
        $obj = $container->get(TestCase5::class);
        static::assertEquals("test.case.5", $obj->child(), "test case 5 的值不正确");
        static::assertEquals("test.case.5.2", $obj->value2, "case case 5.2 未正确注入");
        static::assertEquals($str, $obj->value3, "Autowire 支持别名未生效");
        static::assertEquals("errorAuto", $obj->errorAuto, "Autowire 的无类型说明的变量被覆盖");
        self::assertEquals("test.case.withSet::withSet", $obj->getWithSet(), "setter 方法未被正确调用");
    }

    /**
     * 测试注册信息工厂模式下不传工厂类的问题
     * @return void
     */
    public function testNotEntryFactory()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage("Bean工厂类 " . get_class($this) . " 必须实现" . EntryFactory::class);

        new RegeditInfo(true, self::class, [], true);
    }

    /**
     * 测试has方法
     * @return void
     */
    public function testHas()
    {
        $container = new Container();
        $container->regeditInstance("o", 123);
        $container->regedit("c", RegeditFactory::regeditClass("Cl"));
        static::assertTrue($container->has("o"), "o标识不存在");
        static::assertTrue($container->has(("c")), "c标识不存在");
        static::assertFalse($container->has("i"), "i标识预期不存在但返回存在");
    }

    /**
     * 测试工厂模式下的重建
     * @return void
     */
    public function testRecreate()
    {
        $container = new Container();
        TestFactory::$count = 0;
        $container->regedit("demo", new RegeditInfo(true, TestFactory::class, [], true));
        $o1 = $container->get("demo");
        $o2 = $container->get("r|demo");
        self::assertEquals(2, TestFactory::$count);
        self::assertNotEquals(spl_object_id($o1), spl_object_id($o2));
    }

    /**
     * 测试setter方法
     * @return void
     */
    public function testSetter()
    {
        $container = new Container();
        $mock = $this->getMockForAbstractClass(PropertyAnalysisInterface::class);
        $container->setPropertyAnalysis($mock);
        self::assertEquals(spl_object_id($mock), spl_object_id($container->getPropertyAnalysis()));
    }

    /**
     * 测试make方法
     * @return void
     * @throws NotFoundClass
     */
    public function testMake()
    {
        $container = new Container();
        $obj1 = $container->make(MakeClass::class);
        self::assertEquals("def_onInit", $obj1->def);

        $str = uniqid("random-");
        self::assertEquals("{$str}_onInit", $container->make(MakeClass::class, [$str])->def);
    }

    /**
     * 测试make的异常
     * @return void
     * @throws NotFoundClass
     */
    public function testMakeError()
    {
        $this->expectException(NotFoundClass::class);
        $this->expectErrorMessage("创建实体时类 MakeClass 找不到");
        $container = new Container();
        $container->make("MakeClass");
    }
}

# end of file
