<?php

namespace DreamCat\Container\DemoClass\SubDir;

/**
 * 测试类
 * @author vijay
 */
class SubClass1
{
    /** @var string  */
    public $id;

    /**
     * SubClass1 constructor.
     */
    public function __construct()
    {
        $this->id = uniqid(__CLASS__);
    }
}

# end of file
