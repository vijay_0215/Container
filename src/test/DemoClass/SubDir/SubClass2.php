<?php

namespace DreamCat\Container\DemoClass\SubDir;

/**
 * 测试类
 * @author vijay
 */
class SubClass2
{
    /** @var string  */
    public $id;

    /**
     * @Config db.username
     * @var string $dbuser -
     */
    public $dbuser;

    /**
     * SubClass2 constructor.
     */
    public function __construct()
    {
        $this->id = uniqid(__CLASS__);
    }
}

# end of file
