<?php /** @noinspection PhpUndefinedClassInspection */

namespace DreamCat\Container\DemoClass;

use SplFileObject;

/**
 * 测试类
 * @author vijay
 */
class TestCase5 extends TestCase4
{
    /**
     * @Config test.case.5
     * @var string -
     */
    private $value1;

    /**
     * @Config test.case.5.2
     * @var string -
     */
    public $value2;

    /**
     * @Autowire demo
     * @var mixed $value3
     */
    public $value3;

    /** @var SplFileObject */
    public $testValue;

    /** @Autowire */
    public $errorAuto = "errorAuto";

    /** @Config test.case.withSet */
    private $withSet;

    public function setWithSet($value)
    {
        $this->withSet = $value . "::withSet";
    }

    /**
     * @return mixed
     */
    public function getWithSet()
    {
        return $this->withSet;
    }

    /**
     * @return string
     */
    public function child()
    {
        return $this->value1;
    }
}

# end of file
