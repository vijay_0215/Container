<?php

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\DemoClass\SubDir\SubClass1;
use DreamCat\Container\DemoClass\SubDir\SubClass2 as Ts;

/**
 * 测试用的类
 * @author vijay
 */
class TestCase2
{
    /**
     * @Autowire
     * @var Ts $case1
     */
    private $value1;
    /**
     * @Autowire sub
     * @var mixed $value2
     */
    private $value2;

    /**
     * @Autowire
     * @var SubTestCase $value3
     */
    private $value3;

    /**
     * @Autowire
     * @var TestCase3 $value4
     */
    public $value4;

    /**
     * -
     * @return Ts
     */
    public function value1(): Ts
    {
        return $this->value1;
    }

    /**
     * -
     * @return SubClass1
     */
    public function value2()
    {
        return $this->value2;
    }

    /**
     * -
     * @return SubTestCase
     */
    public function value3()
    {
        return $this->value3;
    }
}

# end of file
