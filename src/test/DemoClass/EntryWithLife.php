<?php

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\EntryLife\EntryOnInit;

/**
 * 带初始化的实体，测试用
 * @author vijay
 */
class EntryWithLife implements EntryOnInit
{
    /** @var EntryOnInit - */
    private $init;

    /**
     * EntryWithLife constructor.
     * @param EntryOnInit $entryOnInit -
     */
    public function __construct(EntryOnInit $entryOnInit)
    {
        $this->init = $entryOnInit;
    }

    /**
     * 容器实体被创建时调用
     * @return void
     */
    public function entryOnInit(): void
    {
        $this->init->entryOnInit();
    }
}

# end of file
