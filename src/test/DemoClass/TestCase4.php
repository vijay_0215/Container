<?php

namespace DreamCat\Container\DemoClass;

/**
 * 测试类
 * @author vijay
 */
class TestCase4
{
    /**
     * @Config test.case.4
     * @var string -
     */
    private $value1;

    /**
     * @Config test.case.4.2
     * @var string -
     */
    public $value2;

    /**
     * @return string
     */
    public function parent()
    {
        return $this->value1;
    }
}

# end of file
