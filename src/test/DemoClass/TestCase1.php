<?php /** @noinspection PhpUnusedPrivateFieldInspection */

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\DemoClass\SubDir\SubClass2;

/**
 * 测试类
 * @author vijay
 */
class TestCase1
{
    /**
     * @Autowire sub
     * @var mixed $public
     */
    public $public;
    /**
     * @Autowire sub2
     * @var SubClass2 $protected
     */
    protected $protected;
    /**
     * @Autowire
     * @var \Exception $private
     */
    private $private;

    /** @var mixed */
    public $noAutowire;
    /** @var int  */
    public $noAutoWithDefault = 100;

    /**
     * @return SubClass2
     */
    public function value1()
    {
        return $this->protected;
    }

    /**
     * @return \Exception
     */
    public function value2()
    {
        return $this->private;
    }
}

# end of file
