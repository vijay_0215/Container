<?php /** @noinspection PhpUnusedPrivateFieldInspection */

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\DemoClass\SubDir\SubClass1;

/**
 * 测试类
 * @author vijay
 */
class SubTestCase extends TestCase1
{
    /**
     * @var SubClass1 $subValue
     * @Autowire
     */
    private $subValue;

    /**
     * @var TestCase1 $parent
     * @Autowire
     */
    private $parent;

    /**
     * @return SubClass1
     */
    public function sub()
    {
        return $this->subValue;
    }

    /**
     * @return TestCase1
     */
    public function par()
    {
        return $this->parent;
    }
}

# end of file
