<?php /** @noinspection PhpDocMissingThrowsInspection */

/** @noinspection PhpUnusedParameterInspection */

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\DemoClass\SubDir\SubClass2;

/**
 * 测试类
 * @author vijay
 */
class TestCase3
{
    /** @var int */
    public $int;
    /** @var float */
    public $float;
    /** @var string */
    public $str;
    /** @var bool */
    public $bool;
    /** @var array */
    public $array;
    /** @var int */
    public $intDef;
    /** @var float */
    public $floatDef;
    /** @var string */
    public $strDef;
    /** @var bool */
    public $boolDef;
    /** @var array */
    public $arrayDef;
    /** @var SubClass2 */
    public $subClass2;

    /**
     * TestCase3 constructor.
     * @param int $int -
     * @param float $float -
     * @param string $str -
     * @param bool $bool -
     * @param array $array -
     * @param SubClass2 $subClass2 -
     * @param int $intDef -
     * @param float $floatDef -
     * @param string $strDef -
     * @param bool $boolDef -
     * @param array $arrayDef -
     */
    public function __construct(
        int $int,
        float $float,
        string $str,
        bool $bool,
        array $array,
        SubClass2 $subClass2,
        int $intDef = 10,
        float $floatDef = 5.3,
        string $strDef = "abc",
        bool $boolDef = true,
        array $arrayDef = ["bbc"]
    ) {
        /** @noinspection PhpUnhandledExceptionInspection */
        $ref = new \ReflectionClass($this);
        foreach ($ref->getProperties() as $property) {
            $name = $property->name;
            /** @noinspection PhpVariableVariableInspection */
            $this->$name = $$name;
        }
    }
}

# end of file
