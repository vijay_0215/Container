<?php
declare(strict_types=1);

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\EntryLife\EntryOnInit;

/**
 * 用来测试make的示例类
 * @author vijay
 */
class MakeClass implements EntryOnInit
{
    /** @var string 标识 */
    public $def;

    /**
     * MakeClass constructor.
     * @param string $def 标识
     */
    public function __construct(string $def = "def")
    {
        $this->def = $def;
    }

    /**
     * @inheritDoc
     */
    public function entryOnInit(): void
    {
        $this->def .= "_onInit";
    }
}

# end of file
