<?php

namespace DreamCat\Container\DemoClass;

/**
 * 测试前缀用的类
 * @author vijay
 */
class TestPrefixClass
{
    /** @var int 对象的自增id */
    public static $idx = 0;

    /** @var int 对象ID */
    public $id;

    /**
     * TestPrefixClass constructor.
     */
    public function __construct()
    {
        $this->id = ++self::$idx;
    }
}

# end of file
