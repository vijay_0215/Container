<?php

namespace DreamCat\Container\DemoClass;

use DreamCat\Container\EntryFactory;
use DreamCat\Container\EntryLife\EntryOnInit;

/**
 * 测试工厂
 * @author vijay
 */
class TestFactory implements EntryFactory, EntryOnInit
{
    public static $count = 0;
    /** @var string 将被注入的ID */
    private $id;

    private $inited = false;

    /**
     * 创建实体
     * @param string $id 实体标识
     * @param string $arg 额外参数
     * @return mixed 实体
     */
    public function create(string $id, string $arg)
    {
        ++self::$count;
        $obj = new \stdClass();
        $obj->fullId = $id;
        $obj->arg = $arg;
        $obj->factoryId = $this->id;
        if (isset($this->other)) {
            $obj->other = $this->other;
        }
        $obj->inited = $this->inited;
        return $obj;
    }

    /**
     * 容器实体被创建时调用
     * @return void
     */
    public function entryOnInit(): void
    {
        $this->inited = true;
    }
}

# end of file
